# OpenML dataset: shuttle

https://www.openml.org/d/40685

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Source: [UCI](https://archive.ics.uci.edu/ml/datasets/Statlog+(Shuttle))

Donor:

Jason Catlett
Basser Department of Computer Science,
University of Sydney, N.S.W., Australia



Data Set Information:

Approximately 80% of the data belongs to class 1. Therefore the default accuracy is about 80%. The aim here is to obtain an accuracy of 99 - 99.9%.

The examples in the original dataset were in time order, and this time order could presumably be relevant in classification. However, this was not deemed relevant for StatLog purposes, so the order of the examples in the original dataset was randomised, and a portion of the original dataset removed for validation purposes.


Attribute Information:

The shuttle dataset contains 9 attributes all of which are numerical. The first one being time. The last column is the class which has been coded as follows :
1 Rad Flow
2 Fpv Close
3 Fpv Open
4 High
5 Bypass
6 Bpv Close
7 Bpv Open


Relevant Papers:

N/A

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40685) of an [OpenML dataset](https://www.openml.org/d/40685). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40685/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40685/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40685/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

